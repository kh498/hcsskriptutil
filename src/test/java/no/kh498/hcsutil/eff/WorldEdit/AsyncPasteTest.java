package no.kh498.hcsutil.eff.WorldEdit;

import ch.njol.skript.lang.Expression;
import ch.njol.skript.lang.SkriptParser;
import ch.njol.skript.lang.util.SimpleExpression;
import ch.njol.util.Kleenean;
import com.sk89q.worldedit.Vector;
import no.kh498.hcsutil.util.Util;
import org.bukkit.Location;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;
import org.junit.Assert;
import org.junit.Test;

public class AsyncPasteTest {

    private static Event testEvt = new Event() {
        @Override
        public HandlerList getHandlers() {
            return null;
        }
    };

    private static class SingularTestExpr<T> extends SimpleExpression {

        private T val;
        private Class<? extends T> clazz;

        private SingularTestExpr(T val, Class<? extends T> clazz) {
            this.clazz = clazz;
        }

        @Override
        public boolean init(Expression<?>[] expressions, int i, Kleenean kleenean, SkriptParser.ParseResult result) {
            return true;
        }

        @Override
        public String toString(Event event, boolean b) {
            return "test";
        }

        @Override
        public boolean isSingle() {
            return true;
        }

        @Override
        public Class<? extends T> getReturnType() {
            return clazz;
        }

        @Override
        protected T[] get(Event event) {
            //noinspection unchecked
            return (T[]) new Object[] {val};
        }
    }


    private static SkriptParser.ParseResult generateResult(int mark) {
        SkriptParser.ParseResult result = new SkriptParser.ParseResult(new SkriptParser(""), "");
        result.mark = mark;
        return result;
    }

    private void testMarks(AsyncPaste ap, boolean expectedPasteAir, boolean expectedhoriCentered,
                           boolean expectedVertCentered) {
        Assert.assertEquals(expectedPasteAir, ap.isPasteAir());
        Assert.assertEquals(expectedhoriCentered, ap.isHorzCentered());
        Assert.assertEquals(expectedVertCentered, ap.isVertCentered());
    }

    private static final Expression[] exprs = {new SingularTestExpr<>("test", String.class),
        new SingularTestExpr<>(new Location(null, 0, 0, 0), Location.class)};

    @Test
    public void init() {
        AsyncPaste ap = new AsyncPaste();

        Assert.assertFalse(ap.init(new Expression[] {null, null}, 0, Kleenean.FALSE, generateResult(0)));

        Assert.assertTrue(ap.init(exprs, 0, Kleenean.FALSE, generateResult(0)));

        /* * * Test if the correct flags are set * * */

        // default, no flag is set
        ap.init(exprs, 0, Kleenean.FALSE, generateResult(0));
        testMarks(ap, true, false, false);

        //no air
        ap.init(exprs, 0, Kleenean.FALSE, generateResult(1));
        testMarks(ap, false, false, false);

        //hori centered
        ap.init(exprs, 0, Kleenean.FALSE, generateResult(2));
        testMarks(ap, true, true, false);

        //no air hori centered
        ap.init(exprs, 0, Kleenean.FALSE, generateResult(3));
        testMarks(ap, false, true, false);

        //vert centered
        ap.init(exprs, 0, Kleenean.FALSE, generateResult(4));
        testMarks(ap, true, false, true);

        //no air vert centered
        ap.init(exprs, 0, Kleenean.FALSE, generateResult(5));
        testMarks(ap, false, false, true);

        //vert and hori centered
        ap.init(exprs, 0, Kleenean.FALSE, generateResult(6));
        testMarks(ap, true, true, true);

        //no air, vertz and hori centered
        ap.init(exprs, 0, Kleenean.FALSE, generateResult(7));
        testMarks(ap, false, true, true);
    }

    @Test
    public void execute() {
        AsyncPaste ap = new AsyncPaste();

        Vector TEN = Vector.ONE.multiply(10);
        Vector NEG_FIVE = Vector.ONE.multiply(-5);

        ap.init(exprs, 0, Kleenean.FALSE, generateResult(0)); //nothing centered
        Assert.assertEquals(Vector.ZERO, Util.center(Vector.ZERO, TEN, ap.isHorzCentered(), ap.isVertCentered()));

        ap.init(exprs, 0, Kleenean.FALSE, generateResult(6)); //vert and horz centered
        Assert.assertEquals(NEG_FIVE, Util.center(Vector.ZERO, TEN, ap.isHorzCentered(), ap.isVertCentered()));

        ap.init(exprs, 0, Kleenean.FALSE, generateResult(2)); //only horz centered
        Assert.assertEquals(NEG_FIVE.setY(0), Util.center(Vector.ZERO, TEN, ap.isHorzCentered(), ap.isVertCentered()));

        ap.init(exprs, 0, Kleenean.FALSE, generateResult(4)); //only vert centered
        Assert.assertEquals(NEG_FIVE.setX(0).setZ(0),
                            Util.center(Vector.ZERO, TEN, ap.isHorzCentered(), ap.isVertCentered()));

        //real world example!
        ap.init(exprs, 0, Kleenean.FALSE, generateResult(2)); //only horz centered
        Vector pos = new Vector(0, 100, 0);
        Vector dim = new Vector(61, 46, 103);
        Vector expected = new Vector(pos.getX() - dim.getX() / 2, pos.getY(), pos.getZ() - dim.getZ() / 2);

        Assert.assertEquals(expected, Util.center(pos, dim, ap.isHorzCentered(), ap.isVertCentered()));
    }


}