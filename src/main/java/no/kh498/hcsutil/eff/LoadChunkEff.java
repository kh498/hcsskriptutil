package no.kh498.hcsutil.eff;

import ch.njol.skript.Skript;
import ch.njol.skript.lang.Effect;
import ch.njol.skript.lang.Expression;
import ch.njol.skript.lang.SkriptParser;
import ch.njol.util.Kleenean;
import no.kh498.hcsutil.util.NullUtil;
import org.bukkit.Location;
import org.bukkit.event.Event;

public class LoadChunkEff extends Effect {


    static {
        Skript.registerEffect(LoadChunkEff.class,

                              "load chunk at %-location%");
    }

    private Expression<Location> locExpr;

    @Override
    protected void execute(final Event e) {
        final Location loc = this.locExpr.getSingle(e);
        if (loc == null) {
            return;
        }
        loc.getChunk().load(true);
    }

    @Override
    public String toString(final Event event, final boolean b) {
        return "load chunk";
    }

    @Override
    public boolean init(final Expression<?>[] expressions, final int i, final Kleenean kleenean,
                        final SkriptParser.ParseResult result) {
        //noinspection unchecked
        this.locExpr = (Expression<Location>) expressions[0];
        return NullUtil.initSuccess(toString(null, true), this.locExpr);
    }
}
