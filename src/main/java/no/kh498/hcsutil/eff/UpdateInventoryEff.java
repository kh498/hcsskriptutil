package no.kh498.hcsutil.eff;

import ch.njol.skript.Skript;
import ch.njol.skript.lang.Effect;
import ch.njol.skript.lang.Expression;
import ch.njol.skript.lang.SkriptParser;
import ch.njol.util.Kleenean;
import no.kh498.hcsutil.util.NullUtil;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;

public class UpdateInventoryEff extends Effect {


    static {
        Skript.registerEffect(UpdateInventoryEff.class,

                              "update [the] inventory of %-player%");
    }

    private Expression<Player> playerExpr;

    @Override
    protected void execute(final Event e) {
        final Player player = this.playerExpr.getSingle(e);
        if (player == null) {
            return;
        }
        player.updateInventory();
    }

    @Override
    public String toString(final Event event, final boolean b) {
        return "updating players inventory";
    }

    @Override
    public boolean init(final Expression<?>[] expressions, final int i, final Kleenean kleenean,
                        final SkriptParser.ParseResult result) {
        //noinspection unchecked
        this.playerExpr = (Expression<Player>) expressions[0];
        return NullUtil.initSuccess(toString(null, true), this.playerExpr);
    }
}
