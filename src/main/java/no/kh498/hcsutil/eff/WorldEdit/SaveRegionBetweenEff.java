package no.kh498.hcsutil.eff.WorldEdit;

import ch.njol.skript.Skript;
import ch.njol.skript.lang.Effect;
import ch.njol.skript.lang.Expression;
import ch.njol.skript.lang.SkriptParser;
import ch.njol.util.Kleenean;
import com.boydti.fawe.object.schematic.Schematic;
import com.sk89q.worldedit.Vector;
import com.sk89q.worldedit.extent.clipboard.io.ClipboardFormat;
import com.sk89q.worldedit.regions.CuboidRegion;
import com.sk89q.worldedit.world.World;
import no.kh498.hcsutil.HCSSU;
import no.kh498.hcsutil.util.NullUtil;
import no.kh498.hcsutil.util.WEUtil;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.event.Event;

import java.io.File;
import java.io.IOException;

public class SaveRegionBetweenEff extends Effect {

    static {
        Skript.registerEffect(SaveRegionBetweenEff.class,
                              "save region (between|from) %-location% (and|to) %-location% as %-string%");
    }

    private Expression<Location> firstLocExpr;
    private Expression<Location> secondLocExpr;

    private Expression<String> schemNameExpr;

    @Override
    protected void execute(Event e) {
        Location firstLoc = firstLocExpr.getSingle(e);
        Location secondLoc = secondLocExpr.getSingle(e);
        File schemFile = WEUtil.getSchematicFile(schemNameExpr.getSingle(e));

        if (schemFile == null || firstLoc == null || secondLoc == null ||
            !firstLoc.getWorld().equals(secondLoc.getWorld())) {
            return;
        }

        Bukkit.getScheduler().runTaskAsynchronously(HCSSU.inst(), () -> {
            Vector firstVec = WEUtil.toVector(firstLoc);
            Vector secondVec = WEUtil.toVector(secondLoc);
            World world = WEUtil.toWorld(firstLoc);

            try {
                CuboidRegion region = new CuboidRegion(world, firstVec, secondVec);
                Schematic schem = new Schematic(region);
                schem.save(schemFile, ClipboardFormat.SCHEMATIC);
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        });
    }

    @Override
    public String toString(Event e, boolean debug) {
        return "save region between";
    }

    @SuppressWarnings("unchecked")
    @Override
    public boolean init(Expression<?>[] exprs, int matchedPattern, Kleenean isDelayed,
                        SkriptParser.ParseResult parseResult) {

        firstLocExpr = (Expression<Location>) exprs[0];
        secondLocExpr = (Expression<Location>) exprs[1];

        schemNameExpr = (Expression<String>) exprs[2];

        return NullUtil.initSuccess(toString(null, true), firstLocExpr, secondLocExpr, schemNameExpr);
    }
}
