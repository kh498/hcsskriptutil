package no.kh498.hcsutil.eff.WorldEdit;

import ch.njol.skript.Skript;
import ch.njol.skript.lang.Effect;
import ch.njol.skript.lang.Expression;
import ch.njol.skript.lang.SkriptParser;
import ch.njol.util.Kleenean;
import com.boydti.fawe.object.schematic.Schematic;
import com.sk89q.worldedit.Vector;
import no.kh498.hcsutil.HCSSU;
import no.kh498.hcsutil.util.NullUtil;
import no.kh498.hcsutil.util.Util;
import no.kh498.hcsutil.util.WEUtil;
import no.kh498.util.BitFlags;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.event.Event;

public class AsyncPaste extends Effect {

    static {
        Skript.registerEffect(AsyncPaste.class,
                              "async [(2¦horizontally|4¦vertically|6¦) centered] paste [schematic] %-string% at " +
                              "%-location% [(0¦with|1¦without) air]");
    }

    //if we should paste air with the schematic
    private boolean pasteAir;

    //if the schematic should be centered at the location
    private boolean horzCentered;
    private boolean vertCentered;

    private Expression<String> schemExpr;
    private Expression<Location> locExpr;


    @Override
    protected void execute(Event e) {
        Location loc = locExpr.getSingle(e);
        Schematic schematic = WEUtil.getSchematic(schemExpr.getSingle(e));

        if (loc == null || schematic == null || schematic.getClipboard() == null) {
            return;
        }

        Bukkit.getScheduler().runTaskAsynchronously(HCSSU.inst(), () -> {

            Vector position = WEUtil.toVector(loc);

            position = Util.center(position, schematic.getClipboard().getDimensions(), horzCentered, vertCentered);
            schematic.paste(WEUtil.toWorld(loc), position, false, pasteAir, null);
        });
    }

    @Override
    public String toString(Event event, boolean b) {
        return "async paste schematic";
    }

    @SuppressWarnings("unchecked")
    @Override
    public boolean init(Expression<?>[] expressions, int i, Kleenean kleenean, SkriptParser.ParseResult result) {

        pasteAir = !BitFlags.test(result.mark, 1);     //decimal 1 is binary 0001
        horzCentered = BitFlags.test(result.mark, 2); //decimal 2 is binary 0010
        vertCentered = BitFlags.test(result.mark, 4);  //decimal 4 is binary 0100


        schemExpr = (Expression<String>) expressions[0];
        locExpr = (Expression<Location>) expressions[1];
        return NullUtil.initSuccess(toString(null, true), schemExpr, locExpr);
    }

    public boolean isPasteAir() {
        return pasteAir;
    }

    public boolean isHorzCentered() {
        return horzCentered;
    }

    public boolean isVertCentered() {
        return vertCentered;
    }
}
