package no.kh498.hcsutil.eff.WorldGuard;

import ch.njol.skript.Skript;
import ch.njol.skript.lang.Effect;
import ch.njol.skript.lang.Expression;
import ch.njol.skript.lang.SkriptParser;
import ch.njol.util.Kleenean;
import com.boydti.fawe.util.EditSessionBuilder;
import com.sk89q.worldedit.BlockVector;
import com.sk89q.worldedit.EditSession;
import com.sk89q.worldedit.blocks.ImmutableDatalessBlock;
import com.sk89q.worldedit.bukkit.BukkitWorld;
import com.sk89q.worldedit.regions.CuboidRegion;
import com.sk89q.worldguard.bukkit.WorldGuardPlugin;
import com.sk89q.worldguard.protection.regions.ProtectedRegion;
import no.kh498.hcsutil.HCSSU;
import no.kh498.hcsutil.util.NullUtil;
import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.event.Event;

public class SetBlocksWGRegionEff extends Effect {

    static {
        Skript.registerEffect(SetBlocksWGRegionEff.class,
                              "(clear|remove) [all] blocks in [(wg|world guard)] region [named] %-string% in [world] " +
                              "%-world%");
    }

    private Expression<String> regionExpr;
    private Expression<World> worldExpr;

    @Override
    protected void execute(Event e) {
        String region = regionExpr.getSingle(e);
        World world = worldExpr.getSingle(e);

        if (world == null) {
            return;
        }


        ProtectedRegion pr = WorldGuardPlugin.inst().getRegionManager(world).getRegion(region);

        if (pr == null) {
            HCSSU.error("Could not find region " + region + " in world " + world.getName());
            return;
        }

        loadChunk(world, pr.getMaximumPoint());
        loadChunk(world, pr.getMinimumPoint());

        Bukkit.getScheduler().runTaskAsynchronously(HCSSU.inst(), () -> {
            com.sk89q.worldedit.world.World wgWorld = new BukkitWorld(world);
            CuboidRegion cubRegion = new CuboidRegion(wgWorld, pr.getMinimumPoint(), pr.getMaximumPoint());
            EditSession editSession = new EditSessionBuilder(world.getName()).fastmode(true).build();

            editSession.setBlocks(cubRegion, new ImmutableDatalessBlock(0));
            editSession.flushQueue();
        });
    }

    private static void loadChunk(World world, BlockVector bv) {
        world.loadChunk(world.getChunkAt(bv.getBlockX(), bv.getBlockZ()));
    }

    @Override
    public String toString(Event e, boolean debug) {
        return "remove all blocks in world guard region";
    }

    @SuppressWarnings("unchecked")
    @Override
    public boolean init(Expression<?>[] exprs, int matchedPattern, Kleenean isDelayed,
                        SkriptParser.ParseResult parseResult) {
        regionExpr = (Expression<String>) exprs[0];
        worldExpr = (Expression<World>) exprs[1];

        return HCSSU.hasWG() && NullUtil.initSuccess(toString(null, true), regionExpr, worldExpr);
    }
}
