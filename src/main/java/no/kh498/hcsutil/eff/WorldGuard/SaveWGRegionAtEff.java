package no.kh498.hcsutil.eff.WorldGuard;

import ch.njol.skript.Skript;
import ch.njol.skript.lang.Effect;
import ch.njol.skript.lang.Expression;
import ch.njol.skript.lang.SkriptParser;
import ch.njol.util.Kleenean;
import com.boydti.fawe.object.schematic.Schematic;
import com.sk89q.worldedit.extent.clipboard.io.ClipboardFormat;
import com.sk89q.worldedit.regions.CuboidRegion;
import com.sk89q.worldedit.world.World;
import com.sk89q.worldguard.bukkit.WorldGuardPlugin;
import com.sk89q.worldguard.protection.ApplicableRegionSet;
import com.sk89q.worldguard.protection.regions.ProtectedRegion;
import no.kh498.hcsutil.HCSSU;
import no.kh498.hcsutil.util.NullUtil;
import no.kh498.hcsutil.util.WEUtil;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.event.Event;

import java.io.File;
import java.io.IOException;

public class SaveWGRegionAtEff extends Effect {

    static {
        Skript.registerEffect(SaveWGRegionNamedEff.class, "save [(wg|world guard)] region at %-location% as %-string%");
    }

    private Expression<Location> locExpr;
    private Expression<String> schemNameExpr;

    @Override
    protected void execute(Event e) {
        Location loc = locExpr.getSingle(e);

        String schemName = schemNameExpr.getSingle(e);
        File schemFile = WEUtil.getSchematicFile(schemName);

        if (loc == null || schemFile == null) {

            return;
        }

        World world = WEUtil.toWorld(loc);
        ApplicableRegionSet regions =
            WorldGuardPlugin.inst().getRegionManager(loc.getWorld()).getApplicableRegions(WEUtil.toVector(loc));

        //only do something if there is only one region
        if (regions.size() != 1) {
            HCSSU.error("Could not save region at %s as there are multiple regions");
            return;
        }

        ProtectedRegion pr = regions.getRegions().iterator().next();

        Bukkit.getScheduler().runTaskAsynchronously(HCSSU.inst(), () -> {
            try {
                CuboidRegion region = new CuboidRegion(world, pr.getMinimumPoint(), pr.getMaximumPoint());
                Schematic schem = new Schematic(region);
                schem.save(schemFile, ClipboardFormat.SCHEMATIC);
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        });
    }

    @Override
    public String toString(Event e, boolean debug) {
        return "save world guard region";
    }

    @SuppressWarnings("unchecked")
    @Override
    public boolean init(Expression<?>[] exprs, int matchedPattern, Kleenean isDelayed,
                        SkriptParser.ParseResult parseResult) {

        locExpr = (Expression<Location>) exprs[0];
        schemNameExpr = (Expression<String>) exprs[1];
        return HCSSU.hasWG() && NullUtil.initSuccess(toString(null, true), locExpr, schemNameExpr);
    }
}
