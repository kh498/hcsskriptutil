package no.kh498.hcsutil.eff.WorldGuard;

import ch.njol.skript.Skript;
import ch.njol.skript.lang.Effect;
import ch.njol.skript.lang.Expression;
import ch.njol.skript.lang.SkriptParser;
import ch.njol.util.Kleenean;
import com.boydti.fawe.object.schematic.Schematic;
import com.boydti.fawe.util.EditSessionBuilder;
import com.sk89q.worldedit.EditSession;
import com.sk89q.worldedit.Vector;
import com.sk89q.worldedit.regions.CuboidRegion;
import com.sk89q.worldguard.bukkit.WorldGuardPlugin;
import com.sk89q.worldguard.protection.regions.ProtectedRegion;
import no.kh498.hcsutil.HCSSU;
import no.kh498.hcsutil.util.NullUtil;
import no.kh498.hcsutil.util.Util;
import no.kh498.hcsutil.util.WEUtil;
import no.kh498.util.BitFlags;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.event.Event;

public class PasteWGRegionAtEff extends Effect {


    static {
        Skript.registerEffect(PasteWGRegionAtEff.class,
                              "[(2¦horizontally|4¦vertically|6¦) centered] [copy[( |-)]]paste [(wg|world[ ]guard)] " +
                              "region [named ]%-string% in [world] %-world% (at|to) %-location% [(0¦with|1¦without) " +
                              "air]");
    }

    private Expression<String> regionExpr;
    private Expression<World> worldExpr;
    private Expression<Location> destLocExpr;

    //if the schematic should be centered at the location
    private boolean horzCentered;
    private boolean vertCentered;

    private boolean pasteAir;

    @Override
    protected void execute(Event e) {
        String orgRegionName = regionExpr.getSingle(e);
        World orgWorld = worldExpr.getSingle(e);
        Location dest = destLocExpr.getSingle(e);

        if (orgRegionName == null || orgWorld == null || dest == null) {
            return;
        }

        ProtectedRegion pr = WorldGuardPlugin.inst().getRegionManager(orgWorld).getRegion(orgRegionName);

        if (pr == null) {
            HCSSU.error("Could not find region " + orgRegionName + " in world " + orgWorld);
            return;
        }

        Bukkit.getScheduler().runTaskAsynchronously(HCSSU.inst(), () -> {
            EditSession copyWorld =
                new EditSessionBuilder(orgWorld.getName()).autoQueue(false).fastmode(true).limitUnlimited()
                                                          .allowedRegionsEverywhere().build();

            EditSession pasteWorld =
                new EditSessionBuilder(WEUtil.toWorld(dest)).autoQueue(false).fastmode(true).limitUnlimited()
                                                            .allowedRegionsEverywhere().build();

            CuboidRegion cubRegion = new CuboidRegion(pr.getMinimumPoint(), pr.getMaximumPoint());
            Schematic schematic = new Schematic(copyWorld.lazyCopy(cubRegion));

            if (schematic.getClipboard() == null) {
                HCSSU.error("The clipboard was null");
                return;
            }
            Vector position = WEUtil.toVector(dest);
            position = Util.center(position, schematic.getClipboard().getDimensions(), horzCentered, vertCentered);

            schematic.paste(pasteWorld, position, false, pasteAir, null);
            pasteWorld.flushQueue();
        });
    }

    @Override
    public String toString(Event e, boolean debug) {
        return "paste world guard region at";
    }

    @SuppressWarnings("unchecked")
    @Override
    public boolean init(Expression<?>[] exprs, int matchedPattern, Kleenean isDelayed,
                        SkriptParser.ParseResult parseResult) {
        regionExpr = (Expression<String>) exprs[0];
        worldExpr = (Expression<World>) exprs[1];
        destLocExpr = (Expression<Location>) exprs[2];

        pasteAir = !BitFlags.test(parseResult.mark, 1);
        horzCentered = BitFlags.test(parseResult.mark, 2); //decimal 2 is binary 0010
        vertCentered = BitFlags.test(parseResult.mark, 4);  //decimal 4 is binary 0100


        return HCSSU.hasWG() && NullUtil.initSuccess(toString(null, true), regionExpr, worldExpr, destLocExpr);
    }
}
