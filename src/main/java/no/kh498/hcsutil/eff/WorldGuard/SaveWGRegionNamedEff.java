package no.kh498.hcsutil.eff.WorldGuard;

import ch.njol.skript.Skript;
import ch.njol.skript.lang.Effect;
import ch.njol.skript.lang.Expression;
import ch.njol.skript.lang.SkriptParser;
import ch.njol.util.Kleenean;
import com.boydti.fawe.object.schematic.Schematic;
import com.sk89q.worldedit.BlockVector;
import com.sk89q.worldedit.bukkit.BukkitWorld;
import com.sk89q.worldedit.extent.clipboard.io.ClipboardFormat;
import com.sk89q.worldedit.regions.CuboidRegion;
import com.sk89q.worldguard.bukkit.WorldGuardPlugin;
import com.sk89q.worldguard.protection.regions.ProtectedRegion;
import no.kh498.hcsutil.HCSSU;
import no.kh498.hcsutil.util.NullUtil;
import no.kh498.hcsutil.util.WEUtil;
import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.event.Event;

import java.io.File;
import java.io.IOException;

public class SaveWGRegionNamedEff extends Effect {

    static {
        Skript.registerEffect(SaveWGRegionNamedEff.class,
                              "save [(wg|world guard)] region named %-string% in [world] %-world% [as %-string%]");
    }

    private Expression<String> regionExpr;
    private Expression<World> worldExpr;
    private Expression<String> schemNameExpr = null;

    @SuppressWarnings("Duplicates")
    @Override
    protected void execute(Event e) {
        String region = regionExpr.getSingle(e);
        World world = worldExpr.getSingle(e);
        String schemName;


        if (schemNameExpr != null) {
            schemName = schemNameExpr.getSingle(e);
        }
        else {
            schemName = region;
        }

        File schemFile = WEUtil.getSchematicFile(schemName);

        if (world == null || schemFile == null) {
            return;
        }


        ProtectedRegion pr = WorldGuardPlugin.inst().getRegionManager(world).getRegion(region);

        if (pr == null) {
            HCSSU.error("Could not find region " + region + " in world " + world);
            return;
        }

        loadChunk(world, pr.getMaximumPoint());
        loadChunk(world, pr.getMinimumPoint());

        Bukkit.getScheduler().runTaskAsynchronously(HCSSU.inst(), () -> {
            try {
                com.sk89q.worldedit.world.World wgWorld = new BukkitWorld(world);
                CuboidRegion cubRegion = new CuboidRegion(wgWorld, pr.getMinimumPoint(), pr.getMaximumPoint());
                Schematic schem = new Schematic(cubRegion);
                schem.save(schemFile, ClipboardFormat.SCHEMATIC);
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        });
    }

    private static void loadChunk(World world, BlockVector bv) {
        world.loadChunk(world.getChunkAt(bv.getBlockX(), bv.getBlockZ()));
    }

    @Override
    public String toString(Event e, boolean debug) {
        return "save world guard region";
    }

    @SuppressWarnings("unchecked")
    @Override
    public boolean init(Expression<?>[] exprs, int matchedPattern, Kleenean isDelayed,
                        SkriptParser.ParseResult parseResult) {
        regionExpr = (Expression<String>) exprs[0];
        worldExpr = (Expression<World>) exprs[1];
        if (exprs.length == 3) {
            schemNameExpr = (Expression<String>) exprs[2];
        }

        return HCSSU.hasWG() && NullUtil.initSuccess(toString(null, true), regionExpr, worldExpr);
    }
}
