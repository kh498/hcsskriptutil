package no.kh498.hcsutil.eff;

import ch.njol.skript.Skript;
import ch.njol.skript.lang.Effect;
import ch.njol.skript.lang.Expression;
import ch.njol.skript.lang.SkriptParser;
import ch.njol.util.Kleenean;
import no.kh498.hcsutil.util.NullUtil;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;

public class PlaySoundAtEff extends Effect {


    static {
        Skript.registerEffect(PlaySoundAtEff.class, "[HCSSU] play %-string% to %-player%");
    }

    private Expression<Player> playerExpr;
    private Expression<String> soundExpr;

    @Override
    protected void execute(final Event e) {
        final Player player = this.playerExpr.getSingle(e);
        final String soundStr = soundExpr.getSingle(e);
        if (player == null || soundStr == null) {
            return;
        }

        Sound s;

        try {
            s = Sound.valueOf(soundStr);
        } catch (IllegalArgumentException ignore) {
            return;
        }
        player.playSound(player.getLocation(), s, 1, 1);
    }

    @Override
    public String toString(final Event event, final boolean b) {
        return "play song to player";
    }

    @SuppressWarnings("unchecked")
    @Override
    public boolean init(final Expression<?>[] expressions, final int i, final Kleenean kleenean,
                        final SkriptParser.ParseResult result) {
        this.soundExpr = (Expression<String>) expressions[0];
        this.playerExpr = (Expression<Player>) expressions[1];
        return NullUtil.initSuccess(toString(null, true), this.playerExpr, soundExpr);
    }
}
