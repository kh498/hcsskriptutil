package no.kh498.hcsutil.eff;

import ch.njol.skript.Skript;
import ch.njol.skript.lang.Effect;
import ch.njol.skript.lang.Expression;
import ch.njol.skript.lang.SkriptParser;
import ch.njol.util.Kleenean;
import no.kh498.hcsutil.util.NullUtil;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;

public class ForceRespawnEff extends Effect {


    static {
        Skript.registerEffect(ForceRespawnEff.class, "[force] respawn [of] %-player%");
    }

    private Expression<Player> playerExpr;

    @Override
    protected void execute(Event e) {
        Player player = playerExpr.getSingle(e);
        if (player != null) {
            player.spigot().respawn();
        }
    }

    @Override
    public String toString(Event e, boolean debug) {
        return "force respawn";
    }

    @Override
    public boolean init(Expression<?>[] exprs, int matchedPattern, Kleenean isDelayed,
                        SkriptParser.ParseResult parseResult) {
        //noinspection unchecked
        playerExpr = (Expression<Player>) exprs[0];
        return NullUtil.initSuccess(toString(null, true), playerExpr);
    }
}
