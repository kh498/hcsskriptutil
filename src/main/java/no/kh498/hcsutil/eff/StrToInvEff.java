package no.kh498.hcsutil.eff;

import ch.njol.skript.Skript;
import ch.njol.skript.lang.Effect;
import ch.njol.skript.lang.Expression;
import ch.njol.skript.lang.SkriptParser;
import ch.njol.util.Kleenean;
import no.kh498.hcsutil.util.InventoryToBase64;
import no.kh498.hcsutil.util.NullUtil;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;

import java.io.IOException;

public class StrToInvEff extends Effect {

    static {
        Skript.registerEffect(StrToInvEff.class, "restore [the] inventory of %-player% from [base64][string] %-string%",
                              "restore %-player%'[s] inventory from [base64][string] %-string%");
    }

    private Expression<Player> playerExpr;
    private Expression<String> stringExpr;

    @Override
    protected void execute(Event e) {
        Player player = playerExpr.getSingle(e);
        String base64Inv = stringExpr.getSingle(e);
        if (player == null || base64Inv == null || base64Inv.isEmpty()) {
            return;
        }
        try {
            InventoryToBase64.restoreTo(player, base64Inv);
        } catch (IOException ignore) {
        }
    }

    @Override
    public String toString(Event e, boolean debug) {
        return "restore players inventory from string";
    }

    @SuppressWarnings("unchecked")
    @Override
    public boolean init(Expression<?>[] exprs, int matchedPattern, Kleenean isDelayed,
                        SkriptParser.ParseResult parseResult) {

        playerExpr = (Expression<Player>) exprs[0];
        stringExpr = (Expression<String>) exprs[1];
        return NullUtil.initSuccess(toString(null, true), playerExpr, stringExpr);
    }
}
