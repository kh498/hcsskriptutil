package no.kh498.hcsutil.expr;

import ch.njol.skript.Skript;
import ch.njol.skript.lang.Expression;
import ch.njol.skript.lang.ExpressionType;
import ch.njol.skript.lang.SkriptParser;
import ch.njol.skript.lang.util.SimpleExpression;
import ch.njol.util.Kleenean;
import org.bukkit.Bukkit;
import org.bukkit.event.Event;

public class MCVersionExpr extends SimpleExpression<String> {


    static {
        Skript.registerExpression(MCVersionExpr.class, String.class, ExpressionType.COMBINED,
                                  "[(1¦simple)] (mc|minecraft) version");
    }

    private boolean useSimple;

    @Override
    protected String[] get(Event e) {
        String rawVer = Bukkit.getServer().getClass().getPackage().getName();
        String version = rawVer.substring(rawVer.lastIndexOf('.') + 1); //returns f.eks "v1_9_R2" or "v1_10_R1"
        if (useSimple) {
            int last = version.lastIndexOf('_');
            version = version.substring(1, last).replace("_", "."); //Returns "1.9" or "1.10"
        }
        return new String[] {version};
    }

    @Override
    public boolean isSingle() {
        return true;
    }

    @Override
    public Class<? extends String> getReturnType() {
        return String.class;
    }

    @Override
    public String toString(Event event, boolean b) {
        return useSimple ? "simple" : "the" + " version of minecraft";
    }

    @Override
    public boolean init(Expression<?>[] expressions, int i, Kleenean kleenean, SkriptParser.ParseResult result) {
        useSimple = result.mark == 1;
        return true;
    }
}
