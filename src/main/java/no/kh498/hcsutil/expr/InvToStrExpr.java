package no.kh498.hcsutil.expr;

import ch.njol.skript.Skript;
import ch.njol.skript.lang.Expression;
import ch.njol.skript.lang.ExpressionType;
import ch.njol.skript.lang.SkriptParser;
import ch.njol.skript.lang.util.SimpleExpression;
import ch.njol.util.Kleenean;
import no.kh498.hcsutil.util.InventoryToBase64;
import no.kh498.hcsutil.util.NullUtil;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;

public class InvToStrExpr extends SimpleExpression<String> {


    static {
        Skript.registerExpression(InvToStrExpr.class, String.class, ExpressionType.COMBINED,
                                  "%player%'[s] inventory as [a] [base64] string",
                                  "inventory of %player% as [a] [base64] string");
    }

    private Expression<Player> playerExpr;

    @Override
    protected String[] get(Event e) {
        Player player = playerExpr.getSingle(e);
        if (player == null) {
            return null;
        }
        return new String[] {InventoryToBase64.toBase64(player.getInventory())};
    }

    @Override
    public boolean isSingle() {
        return true;
    }

    @Override
    public Class<? extends String> getReturnType() {
        return String.class;
    }

    @Override
    public String toString(Event event, boolean b) {
        return "player inventory to string";
    }

    @Override
    public boolean init(Expression<?>[] expressions, int i, Kleenean kleenean, SkriptParser.ParseResult result) {
        //noinspection unchecked
        playerExpr = (Expression<Player>) expressions[0];
        return NullUtil.initSuccess(toString(null, true), playerExpr);
    }
}
