package no.kh498.hcsutil.expr.WorldGuard;

import ch.njol.skript.Skript;
import ch.njol.skript.lang.Expression;
import ch.njol.skript.lang.ExpressionType;
import ch.njol.skript.lang.SkriptParser;
import ch.njol.skript.lang.util.SimpleExpression;
import ch.njol.util.Kleenean;
import com.sk89q.worldguard.bukkit.WorldGuardPlugin;
import com.sk89q.worldguard.domains.DefaultDomain;
import com.sk89q.worldguard.protection.regions.ProtectedRegion;
import no.kh498.hcsutil.HCSSU;
import no.kh498.hcsutil.util.NullUtil;
import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.World;
import org.bukkit.event.Event;

import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

public class MemberOwnerOfRegionExpr extends SimpleExpression<OfflinePlayer> {

    static {
        if (Bukkit.getPluginManager().getPlugin("WorldGuard") == null) {
            HCSSU.error("Could not find plugin WorldGuard");
        }
        else {
            Skript.registerExpression(MemberOwnerOfRegionExpr.class, OfflinePlayer.class, ExpressionType.COMBINED,
                                      "(0¦owner|1¦member)[s] of %-string% in [world] %-world%");
        }
    }

    private boolean owner;
    private Expression<World> worldExpr;
    private Expression<String> regionExpr;


    @Override
    public String toString(Event event, boolean b) {
        return (owner ? "owner" : "member") + "s of region in world";
    }

    @SuppressWarnings("unchecked")
    @Override
    public boolean init(Expression<?>[] expressions, int i, Kleenean kleenean, SkriptParser.ParseResult result) {
        owner = result.mark == 0;
        regionExpr = (Expression<String>) expressions[0];
        worldExpr = (Expression<World>) expressions[1];
        return NullUtil.initSuccess(toString(null, true), regionExpr, worldExpr);
    }

    @Override
    protected OfflinePlayer[] get(Event event) {


        World world = worldExpr.getSingle(event);
        String region = regionExpr.getSingle(event);

        if (world == null || region == null) {
            return null;
        }
        ProtectedRegion wgRegion = WorldGuardPlugin.inst().getRegionManager(world).getRegion(region);

        if (wgRegion == null) {
            HCSSU.error("Could not find region " + region + " in world " + world);
            return null;
        }

        DefaultDomain dd = owner ? wgRegion.getOwners() : wgRegion.getMembers();
        Set<OfflinePlayer> players = new HashSet<>();

        for (UUID uuid : dd.getUniqueIds()) {
            players.add(Bukkit.getOfflinePlayer(uuid));
        }
        for (String name : dd.getPlayers()) {
            players.add(Bukkit.getOfflinePlayer(name));
        }

        return players.toArray(new OfflinePlayer[players.size()]);

    }

    @Override
    public boolean isSingle() {
        return false;
    }

    @Override
    public Class<? extends OfflinePlayer> getReturnType() {
        return OfflinePlayer.class;
    }
}
