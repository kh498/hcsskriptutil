package no.kh498.hcsutil.expr.WorldEdit;

import ch.njol.skript.Skript;
import ch.njol.skript.lang.Expression;
import ch.njol.skript.lang.ExpressionType;
import ch.njol.skript.lang.SkriptParser;
import ch.njol.skript.lang.util.SimpleExpression;
import ch.njol.util.Kleenean;
import com.boydti.fawe.object.schematic.Schematic;
import com.sk89q.worldedit.Vector;
import no.kh498.hcsutil.util.NullUtil;
import no.kh498.hcsutil.util.WEUtil;
import no.kh498.util.BitFlags;
import org.bukkit.event.Event;

public class SizeSchemExpr extends SimpleExpression<Integer> {

    static {
        Skript.registerExpression(SizeSchemExpr.class, Integer.class, ExpressionType.COMBINED,
                                  "(1¦(x|length)|2¦(z|width)|4¦(y|height)) [value] of schematic %-string%");
    }

    private static final int LENGTH_MASK = 0b0001; //1
    private static final int WIDTH_MASK = 0b0010;  //2
    private static final int HEIGHT_MASK = 0b0100; //4

    private int mark;
    private Expression<String> schemExpr;

    @Override
    protected Integer[] get(Event event) {

        Schematic schemFile = WEUtil.getSchematic(schemExpr.getSingle(event));
        if (schemFile == null || schemFile.getClipboard() == null) {
            return null;
        }

        Vector dimensions = schemFile.getClipboard().getDimensions();
        Integer retVal;

        switch (mark) {
            case LENGTH_MASK:
                retVal = dimensions.getBlockX();
                break;
            case WIDTH_MASK:
                retVal = dimensions.getBlockZ();
                break;
            case HEIGHT_MASK:
                retVal = dimensions.getBlockY();
                break;
            default:
                return null;
        }

        return new Integer[] {retVal};
    }

    @Override
    public boolean isSingle() {
        return true;
    }

    @Override
    public Class<? extends Integer> getReturnType() {
        return Integer.class;
    }

    @Override
    public String toString(Event event, boolean b) {
        String length = BitFlags.test(mark, LENGTH_MASK) ? "length " : "";
        String width = BitFlags.test(mark, WIDTH_MASK) ? "width " : "";
        String height = BitFlags.test(mark, HEIGHT_MASK) ? "height " : "";
        return "The " + length + width + height + "of schematic " + (b ? schemExpr.getSingle(event) : "");
    }

    @SuppressWarnings("unchecked")
    @Override
    public boolean init(Expression<?>[] expressions, int i, Kleenean kleenean, SkriptParser.ParseResult result) {
        mark = result.mark;
        schemExpr = (Expression<String>) expressions[0];
        return NullUtil.initSuccess(toString(null, true), schemExpr);
    }
}
