package no.kh498.hcsutil.util;

import no.kh498.hcsutil.HCSSU;

public class NullUtil {

    public static boolean initSuccess(String name, Object... objs) {
        for (int i = 0; i < objs.length; i++) {
            if (objs[i] == null) {
                HCSSU.error("Failed to load " + name + " since expression nr " + i + " was null");
                return false;
            }
        }
        return true;
    }
}
