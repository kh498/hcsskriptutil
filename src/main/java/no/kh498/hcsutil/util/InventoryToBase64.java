package no.kh498.hcsutil.util;

import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.util.io.BukkitObjectInputStream;
import org.bukkit.util.io.BukkitObjectOutputStream;
import org.yaml.snakeyaml.external.biz.base64Coder.Base64Coder;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

public class InventoryToBase64 {

    public static String toBase64(PlayerInventory inv) {
        try {
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            BukkitObjectOutputStream dataOutput = new BukkitObjectOutputStream(outputStream);

            // Save every element in the list
            for (ItemStack item : inv.getContents()) {
                dataOutput.writeObject(item);
            }
            dataOutput.writeObject(inv.getHelmet());
            dataOutput.writeObject(inv.getChestplate());
            dataOutput.writeObject(inv.getLeggings());
            dataOutput.writeObject(inv.getBoots());

            // Serialize that array
            dataOutput.close();

            byte[] data = outputStream.toByteArray();
            return Base64Coder.encodeLines(data, 0, data.length, Integer.MAX_VALUE, "");
//            return Base64Coder.encodeLines(outputStream.toByteArray());
        } catch (Exception e) {
            throw new IllegalStateException("Unable to save item stacks.", e);
        }
    }

    public static void restoreTo(Player player, String data) throws IOException {
        try {
            ByteArrayInputStream inputStream = new ByteArrayInputStream(Base64Coder.decodeLines(data));
            BukkitObjectInputStream dataInput = new BukkitObjectInputStream(inputStream);

            PlayerInventory inventory = player.getInventory();

            // Read the serialized inventory
            for (int i = 0; i < inventory.getSize(); i++) {
                inventory.setItem(i, (ItemStack) dataInput.readObject());
            }

            inventory.setHelmet((ItemStack) dataInput.readObject());
            inventory.setChestplate((ItemStack) dataInput.readObject());
            inventory.setLeggings((ItemStack) dataInput.readObject());
            inventory.setBoots((ItemStack) dataInput.readObject());

            dataInput.close();
        } catch (ClassNotFoundException | IOException e) {
            throw new IOException("Failed to decode inventory");
        }
    }
}
