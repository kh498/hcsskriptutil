package no.kh498.hcsutil.util;

import com.sk89q.worldedit.Vector;

public class Util {

    public static Vector center(Vector position, Vector dimensions, boolean horzCentered, boolean vertCentered) {
        Vector halfDim = dimensions.divide(2);
        if (!horzCentered) {
            halfDim = halfDim.setX(0).setZ(0);
        }
        if (!vertCentered) {
            halfDim = halfDim.setY(0);
        }

        if (horzCentered || vertCentered) {

            //Subtract half of the schematic dimension
            return position.subtract(halfDim);
        }
        return position;
    }
}
