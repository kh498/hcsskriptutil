package no.kh498.hcsutil.util;

import com.boydti.fawe.object.schematic.Schematic;
import com.sk89q.worldedit.Vector;
import com.sk89q.worldedit.bukkit.BukkitWorld;
import com.sk89q.worldedit.bukkit.WorldEditPlugin;
import com.sk89q.worldedit.extent.clipboard.io.ClipboardFormat;
import com.sk89q.worldedit.world.World;
import no.kh498.hcsutil.HCSSU;
import org.bukkit.Bukkit;
import org.bukkit.Location;

import java.io.File;
import java.io.IOException;
import java.net.URI;

public class WEUtil {

    private static final String SCHEMATIC_FOLDER_NAME = "schematics";
    private static final String SCHEMATIC_ENDING = ".schematic";


    private static String schematicPath = null;

    /**
     * @param schematic
     *     The file name, the {@code .schematic} is optional
     *
     * @return The file of a schematic with the name {@code schematic}
     */
    public static File getSchematicFile(String schematic) {
        if (schematic == null) {
            return null;
        }
        if (schematicPath == null) {
            WorldEditPlugin wep = (WorldEditPlugin) Bukkit.getServer().getPluginManager().getPlugin("WorldEdit");
            schematicPath = wep.getDataFolder().toURI() + SCHEMATIC_FOLDER_NAME + '/';
        }
        if (!schematic.endsWith(SCHEMATIC_ENDING)) {
            schematic += SCHEMATIC_ENDING;
        }
        try {
            return new File(URI.create(schematicPath + schematic));
        } catch (Exception ex) {
            HCSSU.error("Could not find the file " + (schematicPath + schematic));
            return null;
        }
    }

    public static Schematic getSchematic(String fileName) {
        try {
            File schemFile = getSchematicFile(fileName);
            if (schemFile == null) {
                return null;
            }
            ClipboardFormat format = ClipboardFormat.findByFile(schemFile);
            if (format == null) {
                HCSSU.errorf("Failed to load file '%s' due to unknown schematic format", schemFile.getAbsolutePath());
                return null;
            }

            return format.load(schemFile);

        } catch (IOException e) {
            HCSSU.errorf("Failed to get the clipboard of '%s'", fileName);
        }
        return null;
    }

    /**
     * @param loc
     *     The location to convert
     *
     * @return The location as a block vector
     */
    public static Vector toVector(Location loc) {
        if (loc == null) {
            return null;
        }
        return new Vector(loc.getX(), loc.getY(), loc.getZ());
    }


    /**
     * @param loc
     *     The location to get the world of
     *
     * @return The {@link BukkitWorld} of the location
     */
    public static World toWorld(Location loc) {
        if (loc == null) {
            return null;
        }
        return new BukkitWorld(loc.getWorld());
    }
}
