package no.kh498.hcsutil;

import ch.njol.skript.Skript;
import ch.njol.skript.SkriptAddon;
import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.IOException;

//HCS Skript Utility
public class HCSSU extends JavaPlugin {

    private static SkriptAddon addonInstance;

    private static HCSSU inst;

    public static HCSSU inst() {
        return inst;
    }

    @Override
    public void onEnable() {
        inst = this;
        register();
    }

    @Override
    public void onDisable() {

    }

    public static void error(final String error) {
        errorf(error);
    }

    public static void errorf(final String error, Object... objects) {
        if (error != null && inst != null) { inst.getLogger().severe(String.format(error, objects)); }
    }

    private static void register() {
        try {
            getAddonInstance().loadClasses(HCSSU.class.getPackage().getName(), "eff", "expr");
        } catch (final IOException e) {
            e.printStackTrace();
        }
    }

    private static SkriptAddon getAddonInstance() {
        if (addonInstance == null) {
            addonInstance = Skript.registerAddon(inst);
        }
        return addonInstance;
    }

    public static boolean hasWE() {
        return hasPlugin("WorldEdit");
    }

    public static boolean hasWG() {
        return hasPlugin("WorldGuard");
    }

    public static boolean hasPlugin(String pluginName) {
        if (!Bukkit.getPluginManager().isPluginEnabled(pluginName)) {
            HCSSU.error("Could not find the plugin '" + pluginName + "'");
            return false;
        }
        return true;
    }
}
