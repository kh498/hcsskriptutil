# HCS Skript Utility

#### [Download link](https://bitbucket.org/kh498/hcsskriptutil/downloads/HCSSU-1.0-SNAPSHOT.jar)

This [Skript](https://github.com/bensku/Skript) addon was intended as a way for me to add small features to the skript language to be used in a yet to be published minecraft project. Therefore there are effects and expressions that can be found in other addons, but for some reason or another could not be used at the time on my server. I have tried to add links to other plugins where the expression/effects already exists. This also means that this addons is only intended as a last resort, when all other plugins fail you this addon will be here waiting for you :3

The expressions I _think_ are unique are ([create an issue](https://bitbucket.org/kh498/hcsskriptutil/issues) if you can prove otherwise)
* Save/load player's inventory as/from a base64 starting
* Copy then paste world guard region
* Using pirateSK syntax to save wg regions
* Center a schematic horizontally/vertically/both

## Requirements:
To use this schematic you **NEED** to have the following plugins

* [Skript](https://github.com/bensku/Skript) _(tested on bensku dev-34 and v9-by-Nfell)_
* [FastAsyncWorldEdit](https://www.spigotmc.org/resources/fast-async-worldedit-voxelsniper.13932/) _(and indirectly WorldEdit)_
* [WorldGuard](https://dev.bukkit.org/projects/worldguard)

## Effects

### Force Respawning of Player ######

Simply respawns the player if he is on the death screen and does nothing otherwise. This effect already exists in [multiple other addons](https://docs.skunity.com/syntax/search/Force%20Respawn)

#### Syntax

```
[force] respawn [of] %player%
```

#### Examples

* `force respawn of player`
* `respawn player`

### Load Chunk ######

Loads a chunk at a location, cannot unload a chunk. This effect already exists in [multiple other addons](https://docs.skunity.com/syntax/search/Load%20Chunk)

#### Syntax

```
load chunk at %location%
```

#### Examples

* `load chunk at player`


### Update Inventory ######

Updates the inventory of a player. This effect already exists in [multiple other addons](https://docs.skunity.com/syntax/search/update%20inventory)

#### Syntax

```
update [the] inventory of %player%
```

#### Examples

* `update inventory of player`


### Async Paste Schematic ######

Using [FAWE](https://github.com/boy0001/FastAsyncWorldedit) to paste schematics fast and asynchronously this method also supports centering the schematic at the location given. By default the effect will paste schematics with air but spesify without air and no air will be pasted.

#### Syntax

```
async [(horizontally|vertically|) centered] paste [schematic] %string% at %location% [(with|without) air]
```

#### Examples

* `async centered paste schematic "testRegion" at player's location with air` Paste the center of the schematic at the player and includes air
* `async horizontally centered paste schematic "testRegion" at player's location` Paste the horizontal center of the schematic at the player and includes air
* `async vertically centered paste schematic "testRegion" at player's location` Paste the vertical center of the schematic at the player and includes air
* `async centered paste schematic "testRegion" at player's location` Paste the center of the schematic at the player and includes air
* `async paste schematic "testRegion.schematic" at player's location without air` Paste schematic at the player but does not include air

### Save Region Between ######

This effect creates a schematic of an area between two locations. The region is saved in the default location `/plugins/worldedit/schematics`. This is done asynchronously with [FAWE](https://github.com/boy0001/FastAsyncWorldedit)

#### Syntax

```
save region (between|from) %location% (and|to) %location% as %string%
```

#### Examples

* `save region between player's location and spawn as "testRegion"`


### Save World Guard Region ######

Save the world guard region at the location as a schematic. If there are multiple regions no region will be saved. The region is saved in the default location `/plugins/worldedit/schematics`. This is done asynchronously with [FAWE](https://github.com/boy0001/FastAsyncWorldedit)

#### Syntax

```
save [(wg|world guard)] region at %location% as %string%
```

#### Examples

* `save wg region at player as "%player%region"`

### Restore inventory of player ######

#### Syntax

```
restore [the] inventory of %player% from [base64][string] %string%
```
```
restore %player%'[s] inventory from [base64][string] %string%
```

#### Examples

* `restore player's inventory from {inv}`


### Copy Paste a World Guard Region ######

Copy a world guard region and paste it at another location. Like `async paste` you can paste the region with or without air and center it horizontally and/or vertically. This syntax is similiar to that of [PirateSk](https://docs.skunity.com/syntax/search/addon:PirateSK)

#### Syntax

```
[(horizontally|vertically|) centered] [copy[( |-)]]paste [(wg|world[ ]guard)] region [named ]%string% in [world] %world% (at|to) %location% [(with|without) air]
```

#### Examples

* `centered copy-paste wg region named "spawn" in world ("World" parsed as world) at (the player's location)`

### Save World Guard region with name and world ######

Save a world guard region as a schematic using the name of a region in a spesfic world. This syntax is similiar to that of [PirateSk](https://docs.skunity.com/syntax/search/addon:PirateSK)

#### Syntax

```
save [(wg|world guard)] region at %location% as %string%
```

#### Examples

* `save wg region at player as "test"`
* `save wg region at player as "test2.schematic"`

## Expressions ############################################

### Inventory of player as base64 string ######

This expression returns a player's inventory as a base64 string. This can be used in combination with [restore inventory of player](#Restore-inventory-of-player)


#### Syntax

```
inventory of %player% as [a ][base64 ]string
```
```
%player%'[s] inventory as [a ][base64 ]string
```

#### Returns

text

#### Examples

* `player's inventory as a base64 string`
* `set {inv} to inventory of player as a base64 string`

### Owners/Members of a region ######

Return the owners and members of a world guard region using name and world of region.

**Note:** This syntax is similiar to that of [PirateSk](https://docs.skunity.com/syntax/search/addon:PirateSK), if you have the region from before you _can_ use the [vanilla expression](https://docs.skunity.com/syntax/search/Region%20Members%20&%20Owners).

**Note:** The vanilla expression linked above does not work with player names (tested on bensku's fork dev-34), this expression does.

#### Syntax

```
(owner|member)[s] of %string% in [world] %world%
```

#### Returns

offline players (aka a list of offline players)

#### Examples

* `owner of "region" in player's world`
* `members of "anotherRegion" in player's world`

### Size of a Schematic ######

Returns either the length, width or height of a schematic. This expression already exists in [SkStuff](https://docs.skunity.com/syntax/search/addon:SkStuff)

#### Syntax

```
((x|length)|(z|width)|(y|height)) [value] of schematic %string%
```

#### Returns

integer

#### Examples

* `length of schematic "island"`
* `y value of schematic "example"`
